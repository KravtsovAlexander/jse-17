package ru.t1.kravtsov.tm.api.service;

import ru.t1.kravtsov.tm.api.repository.IUserOwnedRepository;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M>, IUserOwnedRepository<M> {

    List<M> findAll(String userId, Sort sort);

}
