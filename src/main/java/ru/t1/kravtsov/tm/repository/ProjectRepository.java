package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.model.Project;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
    }

    @Override
    public List<Project> removeByName(final String userId, final String name) {
        final List<Project> removedProjects = new ArrayList<>();
        final Iterator<Project> iterator = findAll(userId).iterator();
        while (iterator.hasNext()) {
            final Project project = iterator.next();
            if (name.equals(project.getName())) {
                iterator.remove();
                removedProjects.add(project);
            }
        }

        return removedProjects;
    }

}
